<?php
session_start();

//echo $_POST['position'];
//echo $_POST['g_res_type'];
//echo $_POST['g_res_text'];
//echo $_SESSION['user_group'];



//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();


if (!isset($_SESSION['userName']))
{
    /*
     * this mimics a login check- if the session variable isn't set, then we send to the start page (in a live
     * system this would be a login page
     */
    header('Location: login.php');
    die();

}


 //set the variable $errMsg to null- this is what we will use to check if errors have been detected later in the page.
$position = null;
$errMsg = null;
$text = null;
$group = null;
$type = null;
// check that data has been posted
if (isset($_POST['position']))
{
    $position = $_POST['position'];
    //$text = $position . "Txt";
}
if(isset($_SESSION['user_group']))
{
    $group = $_SESSION['user_group'];
}

if(isset($_POST['g_res_type']))
{
    $type = $_POST['g_res_type'];
}
if(isset($_POST['g_res_text']))
{
    $text = $_POST['g_res_text'];
}

if ($position && $text && $group && $type)
{
    //validate the message
    if (MsgIsValid($text, 1, 1000))
    {
        //insert the new post to the database
        $handle = CreateHandle();

        $isSubmitted = null;
        
        
        $sql_isSubmitted = "SELECT g_res_text FROM group_response WHERE g_id='$group' AND g_res_position='$position'";
        


        $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
        
        if(count($isSubmitted)<1)
        {
            //escape potentially dangerous characters
        $msg = $handle->real_escape_string($text);
        $group_id = $handle -> real_escape_string($group);
        $g_res_type = $handle -> real_escape_string($_POST['g_res_type']);
        //create the sql statement (note this is particularly insecure)
        $sql = "INSERT INTO group_response (g_id, g_res_position, g_res_text, g_res_type) VALUES ('" . $group_id . "', '" . $position . "', '" .$msg . "', '" . $g_res_type ."')";
        //we can store the result of the query in a variable and then write code to respond depending on the result- this is omitted here for clarity
        $inserted =  $handle -> query($sql) or die(mysqli_error($handle));
          //we will need to refresh the chat page and not re-post data, so we use this page to handle the posted message
        // and then return to the chat page
        echo "done";
        }
        else
        {
            $msg = $handle->real_escape_string($text);
            $g= $handle -> real_escape_string($group);
            
            $sql_update = "UPDATE group_response SET g_res_text='$msg' WHERE g_id='$g' AND g_res_position='$position'";

            if (mysqli_query($handle, $sql_update)) {
                //echo "Record updated successfully";
                echo "don but";
            } else {
                //echo "Error updating record: " . mysqli_error($conn);
            }
        }
        
        //header('Location: first_law_k.php');
    }
    else
    {
        //there is a problem with the characters in the message or the message is blank, so create the error message to display to users
        $errMsg =  "<p>Please check what you have typed- it may contain invalid characters</p>";
        echo "no";
    }
}


//we only render the html page if the chat message doesn't validate.

?>