<?php

session_start();

if (!isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: login.php');
    die();

}
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();
$task = isset($_GET['task']) ? $_GET['task'] : null;
$task_no = ($task) ? substr($task, -1) : null;

?>



<!DOCTYPE html>
<html>


	<head>
		<title>What do you learned about first law</title>
		<meta http-equiv="content-type" 
			content="text/html;charset=utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" language="javascript" src="script.js"></script>
	</head>

	<body>
		<div id="root">
			<div class="header"><h1>What do you learned about first law?</h1></div>
			
			<div class="navigation">
					<a href="home.php">Home</a><br/>
					<a href="logout.php">Logout</a>
					<h2>First Law</h2>
					<ul>
						<li><a href="first_law_k.php">Let's Go</a></li>
						<li><a href="first_law_w.php">I want to ...</a></li>
						<li><a href="first_law_lesson.php">lesson</a></li>
						<li><a href="first_law_l.php">Ok, then...</a></li>
						<ul>
							<li><a href="javascript:undefined" onclick="goToTaskL('firstLT1')">Task 1</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskL('firstLT2')">Task 2</a></li>											
						</ul>
						
					</ul>
			</div>

			<div class="content">
				

				<div id="firstLT0" class="task" style="display: <?php echo MakeItInvisible($task_no, '0')?>">
					<h2>What do you learn from the lessens?</h2>
					<p>Now it is the time to demonstrate your learning about the First Law of Motion.
						Start these tasks to summarize your learning.</p>
						<input type="button" id="firstKHStartBt" value="Start" onclick="goToTaskL('firstLT1')"/>
				</div>

				<div id="firstLT1" class="task" style="display: <?php echo MakeItInvisible($task_no, '1')?>">
				<div class="task">
					<h2>Task 1</h2>
					<h3>Do you find the answer of your questions?</h3>
					<p>Remember <b>your questions that you want to know about the First Law of Motion</b>? It is time to use your learning to answer your own questions.</p>
					<p>Look at your questions one by one and discuss what you learned about them. When reach the answer submit it.
						Don't worry you can edit your responses.</p>

					<?php
					GetQuestions();
					?>
					</div>
					<div class="task">
					<h3>Discussion for Group Questions</h3>
					<p>Discuss your questions with your fellow group members here.</p>
					
					 
    <!-- write out the welcome message -->
   
   
    <form id="chatMessage" name="chatMessage" action="processMsg.php" method="post">

        <div>
        	<input type="hidden" name="task" value="firstL1">
        <label for="newMsg">Your message</label><br/>
        <textarea id="newMsg" name="newMsg" cols="40" rows="5" ><?php echo isset($errMsg) ? htmlspecialchars($_POST['newMsg']) : "" ?></textarea>
            </div>
        
    </form>

    <input type="hidden" id="task" value="firstL1">
    <input type="button" id="chatBtn" value="Add Message" onclick="sendMsg()">
    <p></p>
    <div id="chatContent" calss="scroll"><p id="chatDiv"></p></div>
   

				
				</div>

				</div>
				<div id="firstLT2" style="display: <?php echo MakeItInvisible($task_no, '2')?>">
				<div class="task">
					<h2>Task 2</h2>
					<h3>Are there other things you learned? </h3>
					<p>Discuss with each other about everything you learned from the lessons. If you learned something more,
						that's great. You can add them here.</p>
					
					<?php
					PresentLearned();
					?>

				
				</div>
				<div class="task">
								<h3>Discussion for Group Questions</h3>
								<p>Discuss your questions with your fellow group members here.</p>
								
								 
			    <!-- write out the welcome message -->
			   
			   
			    <form id="chatMessage" name="chatMessage" action="processMsg.php" method="post">

			        <div>
			        	<input type="hidden" name="taskL2" value="firstL2">
			        <label for="newMsg">Your message</label><br/>
			        <textarea id="newMsgL2" name="newMsgL2" cols="40" rows="5" ><?php echo isset($errMsg) ? htmlspecialchars($_POST['newMsg']) : "" ?></textarea>
			            </div>
			        
			    </form>

			    <input type="hidden" id="taskL2" value="firstL2">
			    <input type="button" id="chatBtn" value="Add Message" onclick="sendMsgL2()">
			    <p></p>
			    <div id="chatContentL2" calss="scroll"><p id="chatDivL2"></p></div>
			    

				
				</div>

				
			</div>
</div>
			
			
		</div>
	</body>