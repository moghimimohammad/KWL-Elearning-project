<?php

session_start();


if (!isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: login.php');
    die();

}
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();
$group = $_SESSION['user_group'];
$task = isset($_GET['task']) ? $_GET['task'] : null;
$task_no = ($task) ? substr($task, -1) : null;
?>



<!DOCTYPE html>
<html>


	<head>
		<title>What are our next Steps?</title>
		<meta http-equiv="content-type" 
			content="text/html;charset=utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" language="javascript" src="script.js"></script>
		
	</head>

	<body>
		<div id="root">
			<div class="header"><h1>What I want to know about the First Law of Motion</h1></div>
			
			<div class="navigation">
					<a href="home.php">Home</a><br/>
					<a href="logout.php">Logout</a>
					<h2>First Law</h2>
					<ul>
						<li><a href="first_law_k.php">Let's Go</a></li>
						<li><a href="first_law_w.php">I want to ...</a></li>
						<ul>
							<li><a href="javascript:undefined" onclick="goToTaskW('firstW1')">Task 1</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskW('firstW2')">Task 2</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskW('firstW3')">Task 3</a></li>				
						</ul>
						<li><a href="first_law_lesson.php">lesson</a></li>
						<li><a href="first_law_l.php">Ok, then...</a></li>
					</ul>
			</div>

			<div class="content">
				<div id="firstW0" class="task" style="display: <?php echo MakeItInvisible($task_no, '0')?>">
					<h2>I want to know...</h2>
					<p>Until now, you presented what you know about the <b>"Motion"</b> and subjects that are related to <b>First Law of Motion</b></p>
					<p>At this step you think about what you want to know about this subject. Let's start.</p>
					<input type="button" id="firstKHStartBt" value="Start" onclick="goToTaskW('firstW1')"/>
				</div>
				
				<div id="firstW1" class="task" style="display: <?php echo MakeItInvisible($task_no, '1')?>">
					<h2>Task 1</h2>
					<h3>My questions:</h3>
					<p>Choose between these quetstions those are your question and you want ot know about them. Then submit them and go to next task:</p>
				<form action="WCheckBox.php" method="post">
					<input type="checkbox" name="firstW01" <?php echo IsChecked("firstW01"); ?> value="What is the Inertia?">What is the Inertia?<br>
					<input type="checkbox" name="firstW02" <?php echo IsChecked("firstW02"); ?> value="What is the balanced foreces means?">What is the balanced foreces means?<br>
					<input type="checkbox" name="firstW03" <?php echo IsChecked("firstW03"); ?> value="What happens when an object is acted by unbalanecd forces?">What happens when an object is acted by unbalanecd forces?<br>
					<input type="hidden" name="counter" value="3"/>
					<input type="submit" value="Submit">
				</form>
				</div>

				<div id="firstW2" class="task" style="display: <?php echo MakeItInvisible($task_no, '2')?>">
					<h2>Task 2</h2>
					<h3>Other Questions:</h3>
					<p>If you have other questions that are related to the issue, you can use this textbox to add your question.</p>
				<?php
					PresentQuestions();

					
				?>
				</div>
			

			<div  id="firstW3"  style="display: <?php echo MakeItInvisible($task_no, '3')?>">
				<div class="task">
					<h2>Task 3</h2>
				<h3>Group Activity</h3>
				<p>Here you can see a list of questions that your group submited to find their answers in the next step. Discuss with each other and choose some of these questions.(Maybe some of them are the same or close to each other)</p>
 					<h3>List of Group Questions</h3>
 					<p>Click the <b>Edit</b> button to enable the checkboxes. Again all of you can choose and submit the questions, so discuss with each other to reach the best choices.</p>
 					<?php
 					$handle = CreateHandle();
 					$firstWCounter = 0;
 					$sql_counter = "SELECT * FROM responses WHERE res_position LIKE '%firstW%'";
  					$data = mysqli_query($handle, $sql_counter);

  					$firstWCounter= mysqli_num_rows($data);

				
					 
  $counter = 1;
  $i = 1;
  $done = false;
  while($i< $firstWCounter)
  {
    if($i<10)
    {
    	$pos = "firstW0" . $i;
    }
    else
    {
    	$pos = "firstW" . $i;
    }
//$pos = "firstW0" . $i;
    $handle = CreateHandle();

    $isSubmitted = null;
    $userID = $_SESSION['user_id'];

    $sql_isSubmitted = "SELECT responses.res_text, responses.res_position, responses.fk_user_id, users.user_id, users.user_group FROM responses JOIN users ON responses.fk_user_id=users.user_id WHERE responses.res_position='$pos' AND users.user_group='$group' ORDER BY responses.res_position";
    

    //$sql_isSubmitted = "SELECT res_text, res_position FROM responses WHERE res_position='$pos' ORDER BY res_position";
            


    $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
    if(count($isSubmitted)<1)
    {
      $done = true;
    }
    else
    {
    	$w1Done = false;
    	$w2Done = false;
    	$w3Done = false;
    	for($j=0 ; $j< count($isSubmitted); $j++)
    	{
    		
    		if($counter < 10)
    		{
    			$pos = "firstW0" . $counter;
    		}
    		else
    		{
    			$pos = "firstW" . $counter;
    		}

    		
    		$res = "res" . $pos; 
    		
    			if($isSubmitted[$j][1] == "firstW01" && !$w1Done)
    				{
    					$w1Done = true;
    					echo "<input type=\"checkbox\" id=\"" . $res . "CBD\" name=\"" . $res . "CB\" value=\"" . $isSubmitted[$j][0] . "\" onchage=\"\" disabled/>" . $isSubmitted[$j][0] . "<br/>";
    					$counter++;
    				}
    			elseif($isSubmitted[$j][1] == "firstW02" && !$w2Done)
    			{
    				$w2Done = true;
    				echo "<input type=\"checkbox\" id=\"" . $res . "CBD\" name=\"" . $res . "CB\" value=\"" . $isSubmitted[$j][0] . "\"disabled/>" . $isSubmitted[$j][0] . "<br/>";
    				$counter++;

    			}
    			elseif($isSubmitted[$j][1] == "firstW03" && !$w3Done)
    				{
    					$w3Done = true;
    					echo "<input type=\"checkbox\" id=\"" . $res . "CBD\" name=\"" . $res . "CB\" value=\"" . $isSubmitted[$j][0] . "\"disabled/>" . $isSubmitted[$j][0] . "<br/>";
    					$counter++;
    				}
    				elseif ($isSubmitted[$j][1] == "firstW01" || $isSubmitted[$j][1] == "firstW02" || $isSubmitted[$j][1] == "firstW03") {
    					
    				}
    		
    		else{
    		

		      echo "<input type=\"checkbox\" id=\"" . $res . "CBD\" name=\"" . $res . "CB\" value=\"" . $isSubmitted[$j][0] . "\"disabled/>" . $isSubmitted[$j][0];
 
		      echo "<br/>"; 
		      $counter++;
		      }
		      
      	}
    }
    $i++;  

  }
  $counter--;
  if($counter>0)
  {
	  echo "<p></p>";
	  echo "<input type=\"hidden\" id=\"counter\" value=\"" . $counter . "\"/>";
	  echo "<input type=\"button\" id=\"g_q_submit\" value=\"Edit\" onclick=\"gQSubmit()\"/>";
	}
 					?>
			</div>

							<div class="task">
					<h3>Discussion for Group Questions</h3>
					<p>Discuss your questions with your fellow group members here.</p>
					
					 
    <!-- write out the welcome message -->
   
   
    <form id="chatMessage" name="chatMessage" action="processMsg.php" method="post">

        <div>
        	<input type="hidden" name="task" value="firstW3">
        <label for="newMsg">Your message</label>
        <textarea id="newMsg" name="newMsg" cols="40" rows="5" ><?php echo isset($errMsg) ? htmlspecialchars($_POST['newMsg']) : "" ?></textarea>
            </div>
        
    </form>

    <input type="hidden" id="task" value="firstW3">
    <input type="button" id="chatBtn" value="Add Message" onclick="sendMsg()">
    <p></p>
    <div id="chatContent" calss="scroll"><p id="chatDiv"></p></div>
    

				</div>
				</div>

				

				
		</div>

			
			
		</div>
	</body>