<?php

session_start();


if (!isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: login.php');
    die();

}
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();

$task = isset($_GET['task']) ? $_GET['task'] : null;
$task_no = ($task) ? substr($task, -1) : null;
?>



<!DOCTYPE html>
<html>


	<head>
		<title>What are our next Steps?</title>
		<meta http-equiv="content-type" 
			content="text/html;charset=utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" language="javascript" src="script.js"></script>
	</head>

	<body>
		<div id="root">
			<div class="header"><h1>What I want to know about the First Law of Motion</h1></div>
			
			<div class="navigation">
					<a href="home.php">Home</a>
					<h2>First Law</h2>
					<ul>
						<li><a href="first_law_k.php">Let's Go</a></li>
						<li><a href="first_law_w.php">I want to ...</a></li>
						<ul>
							<li><a href="javascript:undefined" onclick="goToTaskW('firstW1')">Task 1</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskW('firstW2')">Task 2</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskW('firstW3')">Task 3</a></li>				
						</ul>
						<li><a href="first_law_lesson.php">lesson</a></li>
						<li><a href="first_law_l.php">Ok, then...</a></li>
					</ul>
			</div>

			<div class="content">
				<div id="firstW0" class="task" style="display: block">
					<h4>I want to know...</h4>
					<p>Go through these three tasks to relize what you want to know about the First Law of Motion</p>
				</div>
				
				<div id="firstW1" class="task" style="display: none">
					<h3>My questions:</h3>
					<p>You can choose between these questions which are yours:</p>
				<form action="WCheckBox.php" method="post">
					<input type="checkbox" name="firstW1" checked value="What is the Inertia?">What is the Inertia?<br>
					<input type="checkbox" name="firstW2" unchecked value="What is the balanced foreces means?">What is the balanced foreces means?<br>
					<input type="checkbox" name="firstW3" unchecked value="What happens when an object is acted by unbalanecd forces?">What happens when an object is acted by unbalanecd forces?<br>
					<input type="submit" value="Submit">
				</form>
				</div>

				<div id="firstW2" class="task" style="display: none">
				<div class="responseDiv" id="resfirstW4"><div class="response">Who was Newton??<p></p></div><input type="button" value="Edit" onclick="edit('firstW4')"/></div><div id="formfirstW4" style="display: none"><form id="firstW4" name="firstW4" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW4"><textarea id="firstW4Txt" name="firstW4Txt" cols="40" rows="5" >Who was Newton??</textarea></div><input type="submit" id="firstW4Submit" name="firstW4Submit" value="submit your question"></form></div><br/><div class="responseDiv" id="resfirstW5"><div class="response">Another one 5th<p></p></div><input type="button" value="Edit" onclick="edit('firstW5')"/></div><div id="formfirstW5" style="display: none"><form id="firstW5" name="firstW5" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW5"><textarea id="firstW5Txt" name="firstW5Txt" cols="40" rows="5" >Another one 5th</textarea></div><input type="submit" id="firstW5Submit" name="firstW5Submit" value="submit your question"></form></div><br/><p>You can add other questins which you want to learn more about them here</p><form id="firstW6" name="firstW6" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW6"><textarea id="firstW6Txt" name="firstW6Txt" cols="40" rows="5" ></textarea></div><input type="submit" id="firstW6Submit" name="firstW6Submit" value="submit your question"></form>				</div>
			

			<div  id="firstW3" class="task" style="display: none">
				<h4>Group Activity</h4>
 					<div class="responseDiv" id="resfirstW0"><div class="response"><p>What is the Inertia?</p></div><input type="button" value="Edit" onclick="edit('firstW0')"/></div><div id="formfirstW0" style="display: none"><form id="firstW0" name="firstW0" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW0"><textarea id="firstW0Txt" name="firstW0Txt" cols="40" rows="5" >What is the Inertia?</textarea></div><input type="submit" id="firstW0Submit" name="firstW0Submit" value="submit your question"></form></div><br/><div class="responseDiv" id="resfirstW1"><div class="response"><p>What is the Inertia?</p></div><input type="button" value="Edit" onclick="edit('firstW1')"/></div><div id="formfirstW1" style="display: none"><form id="firstW1" name="firstW1" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW1"><textarea id="firstW1Txt" name="firstW1Txt" cols="40" rows="5" >What is the Inertia?</textarea></div><input type="submit" id="firstW1Submit" name="firstW1Submit" value="submit your question"></form></div><br/><div class="responseDiv" id="resfirstW2"><div class="response"><p>What is the balanced foreces means?</p></div><input type="button" value="Edit" onclick="edit('firstW2')"/></div><div id="formfirstW2" style="display: none"><form id="firstW2" name="firstW2" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW2"><textarea id="firstW2Txt" name="firstW2Txt" cols="40" rows="5" >What is the balanced foreces means?</textarea></div><input type="submit" id="firstW2Submit" name="firstW2Submit" value="submit your question"></form></div><br/><div class="responseDiv" id="resfirstW3"><div class="response"><p>Who was Newton??</p></div><input type="button" value="Edit" onclick="edit('firstW3')"/></div><div id="formfirstW3" style="display: none"><form id="firstW3" name="firstW3" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW3"><textarea id="firstW3Txt" name="firstW3Txt" cols="40" rows="5" >Who was Newton??</textarea></div><input type="submit" id="firstW3Submit" name="firstW3Submit" value="submit your question"></form></div><br/><div class="responseDiv" id="resfirstW4"><div class="response"><p>how did Newton find this?</p></div><input type="button" value="Edit" onclick="edit('firstW4')"/></div><div id="formfirstW4" style="display: none"><form id="firstW4" name="firstW4" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW4"><textarea id="firstW4Txt" name="firstW4Txt" cols="40" rows="5" >how did Newton find this?</textarea></div><input type="submit" id="firstW4Submit" name="firstW4Submit" value="submit your question"></form></div><br/><div class="responseDiv" id="resfirstW5"><div class="response"><p>Another one 5th</p></div><input type="button" value="Edit" onclick="edit('firstW5')"/></div><div id="formfirstW5" style="display: none"><form id="firstW5" name="firstW5" action="processResponseW.php" method="post"><div><input type="hidden" name="position" value="firstW5"><textarea id="firstW5Txt" name="firstW5Txt" cols="40" rows="5" >Another one 5th</textarea></div><input type="submit" id="firstW5Submit" name="firstW5Submit" value="submit your question"></form></div><br/>			</div>

				

				<div id="response">
					<h3>Response:</h3>
					<p></p>
				</div>
		</div>

			<div id="pageNav">
				<div id="previous"><a href="task1.html">Previous</a></div>
				<div id="next"><a href="task3.html">Next</a></div>
			</div>
			
		</div>
	</body>