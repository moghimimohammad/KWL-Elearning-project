<?php

session_start();
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';


if (!isset($_SESSION['userName']))
{
    /*
     * this mimics a login check- if the session variable isn't set, then we send to the start page (in a live
     * system this would be a login page
     */
    header('Location: login.php');
    die();

}

UpdateSession();
$handle = CreateHandle();
  
  $i = 4;
  $done = false;
  while($i<7)
  {
    
    $pos = "firstW" . $i;

    $handle = CreateHandle();

    $isSubmitted = null;
    $userID = $_SESSION['user_id'];

    $sql_isSubmitted = "SELECT res_text FROM responses WHERE res_position='$pos'";
            


    $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
    if(count($isSubmitted)<1)
    {
      $done = true;
    }
    else
    {
    	for($j=0 ; $j< $isSubmitted.length; j++)
    	{
	      echo "<div class=\"responseDiv\" id=\"res" . $pos . "\">";
	      echo "<div class=\"response\">";
	      echo "<p>" . FillIt($pos) . "</p>";     
	      echo "</div>";
	      echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>";
	      echo "</div>";  

	      echo "<div id=\"form" . $pos . "\" style=\"display: none\">";
	      echo "<form id=\"firstW" . $i . "\" name=\"firstW" . $i . "\" action=\"processResponseW.php\" method=\"post\">";
	      echo "<div>";
	      echo "<input type=\"hidden\" name=\"position\" value=\"firstW" . $i . "\">";    
	      echo "<textarea id=\"firstW" . $i . "Txt\" name=\"firstW" . $i . "Txt\" cols=\"40\" rows=\"5\" >" . $isSubmitted[0][0] . "</textarea>";    
	      echo "</div>";    
	      echo "<input type=\"submit\" id=\"firstW" . $i . "Submit\" name=\"firstW" . $i . "Submit\" value=\"submit your question\">";     
	      echo "</form>";
	      echo "</div>";  
	      echo "<br/>";  
      	}
    }
    $i++;  

  }

?>

