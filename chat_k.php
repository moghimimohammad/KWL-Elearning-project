<?php

session_start();

if (!isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: login.php');
    die();

}
//notice that we need to call the session_start() function in all pages to use session vars
$task_pos = $_GET['task'];
$_SESSION['task'] = $task_pos;
$type = 'K';
// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();



?>



<!DOCTYPE html>
<html>


	<head>
		<title></title>
		<meta http-equiv="content-type" 
			content="text/html;charset=utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" language="javascript" src="script.js"></script>
	</head>

	<body>
		<div id="root">
			<div class="header"><h1>Discussion Forum</h1></div>
			
			<div class="navigation">
					<a href="home.php">Home</a><br/>
					<a href="logout.php">Logout</a>
					<h2>First Law</h2>
					<ul>
						<li><a href="first_law_k.php">Let's Go</a></li>
						<ul>
							<li><a href="first_law_k.php?task=firstKT1" onclick="goToTask('firstKT1')">Task 1</a></li>
							<li><a href="first_law_k.php?task=firstKT2" onclick="goToTask('firstKT2')">Task 2</a></li>
							<li><a href="first_law_k.php?task=firstKT3" onclick="goToTask('firstKT3')">Task 3</a></li>
							<li><a href="first_law_k.php?task=firstKT4" onclick="goToTask('firstKT4')">Task 4</a></li>
							<li><a href="first_law_k.php?task=firstKT5" onclick="goToTask('firstKT5')">Task 5</a></li>
						</ul>
						<li><a href="first_law_w.php">I want to ...</a></li>
						<li><a href="first_law_lesson.php">lesson</a></li>
						<li><a href="first_law_l.php">Ok, then...</a></li>
					</ul>
			</div>

			<div class="content">
				<div class="task">
					<h2>Task <?php echo substr($task_pos, -1); ?></h2>
					<?php 
					PresentKTask($task_pos);
					?>
					<p>You can discuss with your group member and reach to a response for this task.</p>
					<p>Don't worry, your response should be based on your current knowledge and there is no need to search for the answer, just discuss it with each other. Each of you can submit the answer and edit it, so try it and change it until everybody satisfies.</p>
					<?php 

					$response = IsRespondedG($task_pos);


					

				    echo "<input id=\"task_pos\" type=\"hidden\" name=\"position\" value=\"" . $task_pos . "\">";
					?>
					<form action="" method="post">
						<input type="hidden" name="test" value="hello">
					
						</form>
						<h4>Group Respone</h4>
						<?php
						MakeFormGAJAX($task_pos, 'K');
						?>
				</div>

				<div class="task">
					<h3>Discussion for Task <?php echo substr($task_pos, -1); ?></h3>
					<p>Discuss your ideas with your group mates to reach a better response to the task</p>
					
					 
    <!-- write out the welcome message -->
   
   
    <form id="chatMessage" name="chatMessage" action="processMsg.php" method="post">

        <div>
        	<input type="hidden" name="task" value=<?php echo "\"" . $task_pos . "\""; ?>>
        <label for="newMsg">Your message</label><br/>
        <textarea id="newMsg" name="newMsg" cols="40" rows="5" ><?php echo isset($errMsg) ? htmlspecialchars($_POST['newMsg']) : "" ?></textarea>
            </div>
        
    </form>

    <input type="hidden" id="task" value=<?php echo "\"" . $task_pos . "\""; ?>>
    <input type="button" id="chatBtn" value="Add Message" onclick="sendMsg()">
    <p></p>
    <div id="chatContent" calss="scroll"><p id="chatDiv"></p></div>
    

				</div>
				
			</div>

			
			
		</div>
	</body>