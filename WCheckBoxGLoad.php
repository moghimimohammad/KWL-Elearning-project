<?php

session_start();
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();


if (!isset($_SESSION['userName']))
{
    /*
     * this mimics a login check- if the session variable isn't set, then we send to the start page (in a live
     * system this would be a login page
     */
    header('Location: login.php');
    die();

}


 //set the variable $errMsg to null- this is what we will use to check if errors have been detected later in the page.
$position = null;
$errMsg = null;
$text = null;
$group = null;

$checked = array();
if(isset($_SESSION['user_group']))
{
    $group = $_SESSION['user_group'];
   
}

$counter = (isset($_POST['counter'])) ? $_POST['counter'] : 0;

for($i = 1; $i < $counter + 1; $i++)
{
    if($i<10)
    {
        $pos = "firstW0" . $i;
    }
    else
    {
        $pos = "firstW" . $i;
    }

    //$pos = "firstW" . $i;
   
    if (isset($_POST[$pos]))
    {

    


        

        if (MsgIsValid($_POST[$pos], 1, 1000))
        {
            //insert the new post to the database
            $handle = CreateHandle();

            $isSubmitted = null;
            $userID = $_SESSION['user_id'];

            $sql_isSubmitted = "SELECT g_res_text FROM group_response WHERE g_id='$group' AND g_res_position='$pos'";
            


            $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
            if(count($isSubmitted)<1)
            {
             
            }
            else
            {
                
                
                $checked[] = $pos; 
                
            }
            
            //header('Location: first_law_w.php');
        }
        else
        {
            //there is a problem with the characters in the message or the message is blank, so create the error message to display to users
            $errMsg =  "<p>Please check what you have typed- it may contain invalid characters</p>";
            
        }
    } 
    
}
//header('Location: first_law_w.php');
echo json_encode($checked);

//we only render the html page if the chat message doesn't validate.

?>
