<?php

session_start();

if (!isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: login.php');
    die();

}
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();

?>



<!DOCTYPE html>
<html>


	<head>
		<title>The First Law and our knowledge</title>
		<meta http-equiv="content-type" 
			content="text/html;charset=utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" language="javascript" src="script.js"></script>
	</head>

	<body>
		<div id="root">
			<div class="header"><h1>The First Law and our knowledge</h1></div>
			
			<div class="navigation">
					<a href="home.php">Home</a><br/>
					<a href="logout.php">Logout</a>
					<h2>First Law</h2>
					<ul>
						<li><a href="first_law_k.php">Let's Go</a></li>					
						<li><a href="first_law_w.php">I want to ...</a></li>
						<li><a href="first_law_lesson.php">lesson</a></li>
						<li><a href="first_law_l.php">Ok, then...</a></li>
					</ul>
			</div>

			<div class="content">

				<?php
				
				?>

				<div id="firstK0" class="task" style="display: <?php echo MakeItInvisible($task_no, '0')?>">
					<h3>What do you already know abouth the "Motion"?</h3>
					<p>At this step you would examine your knowledge about the Motion. As you go through these tasks you are able to demonstrate your knowledge.</p>
					<p>There are some directons for you to follow and response to them. There is no need to search anywhere to find things, just consider your knowledge for your responses.</p>
					<p>Ok, are you ready? Press the Start button to go to the tasks and follow directions to complete them.</p>
					<input type="button" id="firstKHStartBt" value="Start" onclick="goToTask('firstKT1')" />
				</div>

				<div id="firstKT1" style="display: <?php echo MakeItInvisible($task_no, '1')?>"> 
					<div class="task">
						<h2>Task 1</h2>
						<h3>The Ball</h3>
						
						<div class="pQuestion">
						<p>Look at this ball, it stays at its position. Do you think it is possible that the ball start to move by itself? What should be happened to move the ball?</p>
						</div>
						<div class="imgQ">
							<img src="Files/ball.jpg" class="imgGrid">
						</div>

						<h4>Your Response:</h4>

						<?php
						
							MakeForm("firstK1", "");
						
						?>
					
					</div>
					
 					<div class="task">
 						<h4>Group Activity</h4>
 						<p>Use this link to go to the discussion forum of this task. Discuss above issue and reach a response and submit it.</p>
 						<a href="chat_k.php?task=firstK1">Chat</a>
 					</div>

				</div>

				<div id="firstKT2" class="task" style="display: <?php echo MakeItInvisible($task_no, '2')?>">
				<div class="task">
					<h2>Task 2</h2>
					<h3>Football Player</h3>
					
					<div class="pQuestion">
					<p>When a football player kick a ball, what causes the ball to move?</p>
					</div>
					<div class="imgQ">
						<img src="Files/kicking.jpg" class="imgGrid">
					</div>
					
					<h4>Your Response:</h4>

					<?php
					
						MakeForm("firstK2", "");
					
					?>

					

					
				</div>

				<div class="task">
 						<h4>Group Activity</h4>
 						<p>Use this link to go to the discussion forum of this task. Discuss above issue and reach a response and submit it.</p>
 						<a href="chat_k.php?task=firstK2">Chat</a>
 					</div>

				</div>

				<div id="firstKT3" class="task" style="display: <?php echo MakeItInvisible($task_no, '3')?>">
				<div class="task">
					<h2>Task 3</h2>
					<h3>The Ball and astronaut</h3>					

					<div class="pQuestion">
					<p>Whatch this video, then explain what would happens if an astronaut throws a ball into the space?</p>

					</div>
					<div class="videoGridCorner">
						
						<video id="video1" style="width:300px;max-width:100%;" controls="">
						    <source src="https://upload.wikimedia.org/wikipedia/commons/transcoded/1/15/Station_Astronauts_Do_Experiment_for_%27Cosmos%27.webm/Station_Astronauts_Do_Experiment_for_%27Cosmos%27.webm.360p.webm" type="video/webm">
						    
						    Your browser does not support HTML5 video.
						</video>
					</div>
					
					<?php
					
						MakeForm("firstK3", "");
					
					?>


				</div>

				<div class="task">
 						<h4>Group Activity</h4>
 						<p>Use this link to go to the discussion forum of this task. Discuss above issue and reach a response and submit it.</p>
 						<a href="chat_k.php?task=firstK3">Chat</a>
 					</div>

				</div>

				<div id="firstKT4" class="task" style="display: <?php echo MakeItInvisible($task_no, '4')?>">
				<div class="task">
					<h2>Task 4</h2>
					<h3>What do you know about inertia?</h3>
					<p>Do you know what inertia is? Write anything which it bears in your mind.</p>
					<h4>Your Response:</h4>

					<?php
					
						MakeForm("firstK4", "");
					
					?>

					

				</div>
					<div class="task">
 						<h4>Group Activity</h4>
 						<p>Use this link to go to the discussion forum of this task. Discuss above issue and reach a response and submit it.</p>
 						<a href="chat_k.php?task=firstK4">Chat</a>
 					</div>

				</div>

				<div id="firstKT5" class="task" style="display: <?php echo MakeItInvisible($task_no, '5')?>">
				<div class="task">
					<h2>Task 5</h2>
					<h3>The Definition</h3>
					<p>This is the definition of <b>First Law of Motion</b>, read it and response to these questions:</p>
					<p class="definition"><b>An object at rest stays at rest and an object in motion stays in motion with the same speed and in the same direction unless acted upon by an unbalanced force</b></p>
					<p>What this definition means for you? Do you know the terms which are used in it?</p>
					<p>Write anything you know and keep in mind your questions for the next session.</p>
					<h4>Your Response:</h4>

					<?php
					
						MakeForm("firstK5", "");
					
					?>

					

				</div>

					<div class="task">
 						<h4>Group Activity</h4>
 						<p>Use this link to go to the discussion forum of this task. Discuss above issue and reach a response and submit it.</p>
 						<a href="chat_k.php?task=firstK5">Chat</a>
 					</div>

				</div>
				

				
			</div>

			
			
		</div>
	</body>