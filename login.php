<?php
session_start();
//notice that we need to call the session_start() function in all pages to use session vars

  // include common functions that other pages will need.
require_once 'app_lib.php';

//here we mimic a login page by setting the user (simply by asking for a name)

/* How this page works
 * If they have not 'logged in' they will not have a session variable set for them.
 *
 * If they have 'logged in' they will already has a session variable, so they are
 * redirected to the chat page.
 *
 * If they don't have a session variable set, we are interested in what name they have posted
 *
 * If the name is valid (only contains letters and numbers), we create the session variable for them and then
 * redirect them to the next page
 *
 * If the name is invalid, or they have not yet posted a name, we show them the form and ask them to provide a name
 * (or correct the one they did provide).
 *
 * Ultimately we want a valid name that we can store in a session variable ('user') before we will let them chat
 * (notice that in the other pages, if the session variable 'user' is not set, the user will be redirected back to this page
 * for that reason.
 *
 *
 */


if (isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: home.php');
    die();

}
elseif (isset($_POST['user']))
{
    //a name has been posted using the form, is it valid?
    if(!isset($_POST['group']))
        {
            $group = '1';
        }
        else
        {
            $group = $_POST['group'];
        }
    if (isValid($_POST['user'], 1, 50) && isValid($group,1,2))
    {
        //set the user's name for this session
        $_SESSION['userName'] = $_POST['user'];
            //having done this, redirect them

        $handle = CreateHandle();

        $user = $handle->real_escape_string($_POST['user']);
        $group = $handle->real_escape_string($group);

        $sql_check = "SELECT user_name FROM users WHERE user_name='$user' And user_group='$group'";

        $registered = queryDB($handle, $sql_check);

    if (count($registered)<1)
    {
        //escape potentially dangerous characters
        
        //create the sql statement (note this is particularly insecure)
        $sql = "INSERT INTO users (user_name, user_group) VALUES ('" . $user . "', '" . $group . "')";
        //we can store the result of the query in a variable and then write code to respond depending on the result- this is omitted here for clarity
        $inserted =  $handle -> query($sql) or die(mysqli_error($handle));
    }
        
       


        header('Location: home.php');
        die();

        }
}
 //if the user isn't already set or the user name they have posted is invalid (invalid chars for example), or this is not a postback,
//then render the page

?>
<!doctype html>
<html>
<head>
    <title>Login</title>
   
        <title>Home</title>
        <meta http-equiv="content-type" 
            content="text/html;charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" language="javascript" src="script.js"></script>
   

</head>
<body>


<div id="root">
            <div class="header"><h1>Login</h1></div>
            
            <div class="navigation">
                    <a href="home.php">Home</a>
                    <h2>Welcome!</h2>
                    
            </div>

            <div class="content">

               

                <div class="task">




<p>Log in here</p>

<form id="setName" name="setName" action="login.php" method="post">
    <div>
        <?php

        ?>
        <label for="user">User Name&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
        <input id="user" name='user' type="text"
               pattern="[A-Za-z0-9]{1,50}" <?php echo isset($_POST['user']) ? "'value = '" . htmlspecialchars($_POST['user']) . "'" : '' ?>>
               <p></p>
        <label for="group">Group Number</label>
        <input id="user" name='group' type="text"
               pattern="[0-9]{1,2}" <?php echo isset($_POST['group']) ? "'value = '" . htmlspecialchars($_POST['group']) . "'" : '' ?>>
    <p>For your group number use a number between 1 to 99.</p>
    </div>
    
    <input type="submit" id="cmdSubmit" name="cmdSubmit" value="login">
</form>
</div>
</div>
</body>
</html>