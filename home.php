<?php

session_start();



require_once 'app_lib.php';

UpdateSession();

 ?>

<!DOCTYPE html>
<html>


	<head>
		<title>Home</title>
		<meta http-equiv="content-type" 
			content="text/html;charset=utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" language="javascript" src="script.js"></script>
	</head>

	<body>
		<div id="root">
			<div class="header"><h1>Newton First Law of Motion</h1></div>
			
			<div class="navigation">
					<a href="home.php">Home</a><br/>
					<a href="login.php">Login</a><br/>
					<a href="logout.php">Logout</a>
					<h2>First Law</h2>
					<ul>
						<li><a href="first_law_k.php">Let's Go</a></li>
						<li><a href="first_law_w.php">I want to ...</a></li>
						<li><a href="first_law_lesson.php">lesson</a></li>
						<li><a href="first_law_l.php">Ok, then...</a></li>
					</ul>
			</div>

			<div class="content">
				<div class="task">
					<h2>Descriptiom</h2>
					<p>This is an E-Learning Application is developed for learning the First Law of Motion. The learning strategy that is 
						used is <b>"What I know, what I want to know and what I learned(KWL)"</b>. </p>
					<p>The learners have the opportunity to collaborate with each other during their studies to share their knowledge and
						help each other to have a better understanding of the subject</p>
					
				

				
					
					<p>Click Start or use navigation bar to go to the sessions</p>
				</div>
			</div>

			<div id="pageNav">
				
				<div id="next"><a href="first_law_k.php">Start</a></div>
			
			</div>
		</div>
	</body>