<?php

session_start();
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();


if (!isset($_SESSION['userName']))
{
    /*
     * this mimics a login check- if the session variable isn't set, then we send to the start page (in a live
     * system this would be a login page
     */
    header('Location: login.php');
    die();

}


 //set the variable $errMsg to null- this is what we will use to check if errors have been detected later in the page.
$position = null;
$errMsg = null;
$text = null;
$group = null;
$type = null;
// check that data has been posted
if (isset($_POST['position']))
{
    $position = $_POST['position'];
    $text = $position . "Txt";
}
if(isset($_SESSION['user_group']))
{
    $group = $_SESSION['user_group'];
}

if(isset($_POST['g_res_type']))
{
    $type = $_POST['g_res_type'];
}
if ($position && $text && $group && $type && isset($_POST[$text]))
{
    //validate the message
    if (MsgIsValid($_POST[$text], 1, 1000))
    {
        //insert the new post to the database
        $handle = CreateHandle();

        $isSubmitted = null;
        

        $sql_isSubmitted = "SELECT g_res_text FROM group_response WHERE g_id='$group' AND g_res_position='$position'";
        


        $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
        
        if(count($isSubmitted)<1)
        {
            //escape potentially dangerous characters
        $msg = $handle->real_escape_string($_POST[$text]);
        $group_id = $handle -> real_escape_string($_POST['user_group']);
        $g_res_type = $handle -> real_escape_string($_POST['g_res_type']);
        //create the sql statement (note this is particularly insecure)
        $sql = "INSERT INTO group_response (g_id, g_res_position, g_res_text, g_res_type) VALUES ('" . $group_id . "', '" . $position . "', '" .$msg . "', '" . $g_res_type ."')";
        //we can store the result of the query in a variable and then write code to respond depending on the result- this is omitted here for clarity
        $inserted =  $handle -> query($sql) or die(mysqli_error($handle));
          //we will need to refresh the chat page and not re-post data, so we use this page to handle the posted message
        // and then return to the chat page
        }
        else
        {
            $msg = $handle->real_escape_string($_POST[$text]);
            $user_id = $handle -> real_escape_string($_SESSION['user_id']);
            
            $sql_update = "UPDATE group_response SET g_res_text='$msg' WHERE g_id='$group' AND g_res_position='$position'";

            if (mysqli_query($handle, $sql_update)) {
                //echo "Record updated successfully";
            } else {
                //echo "Error updating record: " . mysqli_error($conn);
            }
        }
        
        header('Location: first_law_k.php');
    }
    else
    {
        //there is a problem with the characters in the message or the message is blank, so create the error message to display to users
        $errMsg =  "<p>Please check what you have typed- it may contain invalid characters</p>";
    }
}


//we only render the html page if the chat message doesn't validate.

?>
<!doctype html>
    <html>
<head>
    <title>Chat</title>

    <style type="text/css">


    </style>
</head>
    <body>
    <h1>Chat example</h1>



    <?php
    echo $errMsg;
//this form is identical to the one in chat.php, but here we are only handling messages that have errors
     //we need this  separate page to handle the messages being posted (or re-posted if there are errors) so that the
    // chat.php page can safely be refreshed.

    
                    echo "<div id=\"form" . $position . "\">";
                    
                    echo "<form id=\"" . $position . "\" name=\"" . $position . "\" action=\"groupResponse.php\" method=\"post\">";
                    echo "<div>";
                    echo "<input type=\"hidden\" name=\"user_group\" value=\"" . $group . "\">";
                    echo "<input type=\"hidden\" name=\"g_res_type\" value=\"" . $type . "\">";
                    echo "<input type=\"hidden\" name=\"position\" value=\"" . $position . "\">";    
                    echo "<textarea id=\"" . $position . "Txt\" name=\"" . $position . "Txt\" cols=\"40\" rows=\"5\" ></textarea>";    
                    echo "</div>";    
                    echo "<input type=\"submit\" id=\"" . $position . "Submit\" name=\"" . $position . "Submit\" value=\"submit your question\">";     
                    echo "</form>";  
                    echo "</div>";
                    ?>
    </body>
    </html>