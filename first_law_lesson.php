<?php

session_start();

if (!isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: login.php');
    die();

}
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();
$task = isset($_GET['task']) ? $_GET['task'] : null;
$task_no = ($task) ? substr($task, -1) : null;

?>



<!DOCTYPE html>
<html>


	<head>
		<title>The First Law and our knowledge</title>
		<meta http-equiv="content-type" 
			content="text/html;charset=utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" language="javascript" src="script.js"></script>
	</head>

	<body>
		<div id="root">
			<div class="header"><h1>The First Law Lessons</h1></div>
			
			<div class="navigation">
					<a href="home.php">Home</a><br/>
					<a href="logout.php">Logout</a>
					<h2>First Law</h2>
					<ul>
						<li><a href="first_law_k.php">Let's Go</a></li>
						<li><a href="first_law_w.php">I want to ...</a></li>
						<li><a href="first_law_lesson.php">lesson</a></li>
						<ul>
							<li><a href="javascript:undefined" onclick="goToTaskLesson('firstLesson1')">Task 1</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskLesson('firstLesson2')">Task 2</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskLesson('firstLesson3')">Task 3</a></li>
							<li><a href="javascript:undefined" onclick="goToTaskLesson('firstLesson4')">Task 4</a></li>				
						</ul>
						<li><a href="first_law_l.php">Ok, then...</a></li>
					</ul>
			</div>

			<div class="content">
				<div id="firstLesson0" class="task" style="display: <?php echo MakeItInvisible($task_no, '0')?>">
					<h2>Lessons</h2>
					<p>Now it is time to learn more about the First Law of Motion. Until now you discuss what you know and what you want to know with your group
						and maybe learn something from each other.</p>
					<p>Go through Task 1 to 4, whatch the videos and find answers to your questions and new knowledge of the subject.</p>
					<input type="button" id="firstKHStartBt" value="Start" onclick="goToTaskLesson('firstLesson1')"/>
				</div>

				<div id="firstLesson1" class="task" style="display: <?php echo MakeItInvisible($task_no, '1')?>">
					<h2>Task 1</h2>
					<h3>Newton in The Space</h3>
					<p>This video shows you the concepts of the First Law of Motion. It is recorded in the space station where the gravity is almost zero
					and it helps you have better understanding of the motion. Don't worry if you won't understand everything clearly, in the next tasks
					they would be stated clearly.</p>
					<iframe class="videoGrid" src="https://www.youtube.com/embed/sujBQnMnrRU?end=417&rel=0" frameborder="0" allowfullscreen data-youtubeid="5-ZFOhHQS68" data-translatedyoutubeid="5-ZFOhHQS68" data-translatedyoutubelang="en" tabindex="0" data-reactid=".1k5q5187qww.2.0.0.0.$5-ZFOhHQS68.1.0.0"></iframe>
					<br/>
					
					
				</div>

				<div id="firstLesson2" class="task" style="display: <?php echo MakeItInvisible($task_no, '2')?>">
					<h2>Tqsk 2</h2>
					<h3>Newton Law of Motion</h3>
					<p>Here there are information about the First Law history, the notion of people about the motion and new statements about it.</p>
					<iframe class="videoGrid" id="video_5-ZFOhHQS68" type="text/html" frameborder="0" allowfullscreen="" src="//www.youtube.com/embed/5-ZFOhHQS68/?controls=1&amp;enablejsapi=1&amp;modestbranding=1&amp;showinfo=0&amp;origin=https%3A%2F%2Fwww.khanacademy.org&amp;iv_load_policy=1&amp;html5=1&amp;autoplay=0&amp;fs=1&amp;rel=0&amp;hl=en&amp;cc_load_policy=1&amp;start=0" width="800" height="405" data-youtubeid="5-ZFOhHQS68" data-translatedyoutubeid="5-ZFOhHQS68" data-translatedyoutubelang="en" tabindex="0" data-reactid=".1k5q5187qww.2.0.0.0.$5-ZFOhHQS68.1.0.0"></iframe>
					<br/>
					
				</div>

				<div id="firstLesson3" class="task" style="display: <?php echo MakeItInvisible($task_no, '3')?>">
					<h2>Task 3</h2>
					<h3>Newton First Law Concepts</h3>
					<p>Now you are familiar with the First Law of motion. 
						Let's become deeper.</p>
					<iframe class="videoGrid" id="video_D1NubiWCpQg" type="text/html" frameborder="0" allowfullscreen="" src="//www.youtube.com/embed/D1NubiWCpQg/?controls=1&amp;enablejsapi=1&amp;modestbranding=1&amp;showinfo=0&amp;origin=https%3A%2F%2Fwww.khanacademy.org&amp;iv_load_policy=1&amp;html5=1&amp;autoplay=0&amp;fs=1&amp;rel=0&amp;hl=en&amp;cc_load_policy=1&amp;start=0" width="800" height="405" data-youtubeid="D1NubiWCpQg" data-translatedyoutubeid="D1NubiWCpQg" data-translatedyoutubelang="en" tabindex="-1" data-reactid=".1k5q5187qww.2.0.0.0.$D1NubiWCpQg.1.0.0"></iframe>

					
				</div>

				<div id="firstLesson4" class="task" style="display: <?php echo MakeItInvisible($task_no, '4')?>">
					<h2>Task 4</h2>
					<h3>Newton First Law of Motion</h3>
					<p>Do you remember the definition of the First Law of Motion? Now it is the time to explain it.</p>
					
					<iframe class="videoGrid" id="video_CQYELiTtUs8" type="text/html" frameborder="0" allowfullscreen="" src="//www.youtube.com/embed/CQYELiTtUs8/?controls=1&amp;enablejsapi=1&amp;modestbranding=1&amp;showinfo=0&amp;origin=https%3A%2F%2Fwww.khanacademy.org&amp;iv_load_policy=1&amp;html5=1&amp;autoplay=0&amp;fs=1&amp;rel=0&amp;hl=en&amp;cc_load_policy=1&amp;start=0" width="800" height="480" data-youtubeid="CQYELiTtUs8" data-translatedyoutubeid="CQYELiTtUs8" data-translatedyoutubelang="en" tabindex="0" data-reactid=".9lgsu166m8.2.0.0.0.$CQYELiTtUs8.1.0.0"></iframe>
					
				</div>

				
			</div>

					

			
			
		</div>
	</body>