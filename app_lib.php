<?php


function UpdateSession()
{
  $handle = CreateHandle();

if(isset($_SESSION['userName']) && !empty($_SESSION['userName'])){
  $sql_user_id = "SELECT user_id, user_name, user_group FROM users WHERE user_name='" . $_SESSION['userName'] . "'";

  $user_id = queryDB($handle, $sql_user_id);

  $_SESSION['user_id'] = $user_id[0][0];
  $_SESSION['user_group'] = $user_id[0][2];
  }
}




function IsValid($str, $min, $max)
{
    //args: a string to validate with its minimum and maximum length of the string    //names will initially come from a name submitted in the form
    $regex = '/^[A-Za-z0-9]{' . $min . ',' . $max . '}$/'; // this is then stored in a session variable
    return preg_match($regex, $str);
}

function MsgIsValid($str, $min, $max)
{
    //args: a string to validate with its minimum and maximum length of the string    //names will initially come from a name submitted in the form
    //the difference between this and the IsValid() function is the regular expression- here it allows a greater range of characters.
    $regex = '/^[A-Za-z0-9\s\.\,\?\!]{' . $min . ',' . $max . '}$/';
    return preg_match($regex, $str);
}

function CreateHandle()
{
     //provides a handle for the database
	 //put in your own credentials here.
    $host = "localhost";
    $user = "mm15agp";
    $pass = "xDU7Vr8Q";
    $db = "dbmm15agp";


    $mysqli = new mysqli($host, $user, $pass, $db);
    if (mysqli_connect_errno()) {
        //connection problem
        echo "<p> connection problem</p>";
        return false;
    } else {

        return $mysqli;
    }

}
function queryDB($linkIdentifier, $sqlString)
{
    $r = null;
      //use the $res var to store the result of the query
    $res = mysqli_query($linkIdentifier, $sqlString);

      //now get results set back

    if (!$res)
    {
       // nothing returned
        return false;
    }
     //if we get here, then the query worked, but did it return any rows?
    $numberOfRows = mysqli_num_rows($res);

    if ($numberOfRows>0)
    {
        $r = array();

           //we iterate over $row in order to populate our own array that we then return to the calling code
        while($row = mysqli_fetch_array($res))
        {
            $r[] = $row;
        }

     }
    else
    {
        // no results found
        // $r will be returned with the default value of null so nothing to add here.

    }
     //clean up 
    mysqli_free_result($res);
    return $r;
}

function MyQueryDB($linkIdentifier, $sqlString)
{
    $r = null;
      //use the $res var to store the result of the query
    $res = mysqli_query($linkIdentifier, $sqlString);

      //now get results set back

    if (!$res)
    {
       // nothing returned
        return null;
    }
     //if we get here, then the query worked, but did it return any rows?
    $numberOfRows = mysqli_num_rows($res);

    if ($numberOfRows>0)
    {
        $r = array();

           //we iterate over $row in order to populate our own array that we then return to the calling code
        while($row = mysqli_fetch_array($res))
        {
            $r[] = $row;
        }

     }
    else
    {
        // no results found
        // $r will be returned with the default value of null so nothing to add here.

    }
     //clean up 
    mysqli_free_result($res);
    return $r;
}



function GetChatData($group, $task_pos)
{
   //get the messages submitted so far

  $handle = CreateHandle();


    $sql = "SELECT chat_msg_text, postedBy, timedAt FROM tasks_chat_msgs WHERE chat_msg_pos='$task_pos' AND chat_msg_group='$group' ORDER BY timedAt";
       //pass the queryDB() function the handle and sql statement we need to access the database and retrieve existing messages
    $chatMsgs = queryDB($handle, $sql);

    if (count($chatMsgs)<1)
    {
       echo "No messages have been posted, be the first to post one!";
    }
    else
    {
        echo "Please add your message below";
          //now output all the messages to the page
        for ($i=0; $i<count($chatMsgs); $i++)
        {
              //format the date so we are only showing the time the chat message was posted.
            $dateTimePosted = strtotime( $chatMsgs[$i][2]);
            $showDate = date('H:i', $dateTimePosted);
              //write the results (unstyled) to the page
            echo "<div class=\"comment\"><p>" . $chatMsgs[$i][1] . " said: <br>" . $chatMsgs[$i][0] . "<br>" . $showDate . "</p></div>";
        }
    }

}

function MakeForm($pos, $type)
{
  $response = IsResponded($pos);
  if(!$response)
  {
    echo "<div class=\"responseDiv\" id=\"res" . $pos . "\" style=\"display: none\">";
    
    echo "<div class=\"response\">";
    echo "<p>This must be hidden</p>";          
    echo "</div>";  
    echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>"; 
    echo "</div>";       

    echo "<div id=\"form" . $pos . "\">";
    
    echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponse" . $type . ".php\" method=\"post\">";
    echo "<div>";
    echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
    echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" ></textarea>";    
    echo "</div>";    
    echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"Submit\">";     
    echo "</form>";  
    echo "</div>";    
  }
  else
  {
    echo "<div class=\"responseDiv\" id=\"res" . $pos . "\">";
    
    echo "<div class=\"response\">";
    echo "<p>" . FillIt($pos) . "</p>";
    echo "</div>";
    echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>"; 
    echo "</div>"; 


    echo "<div id=\"form" . $pos . "\" style=\"display: none\">";
    
    echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponse" . $type . ".php\" method=\"post\">";
    echo "<div>";
    echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
    echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" >" . $response[0][0] . "</textarea>";    
    echo "</div>";    
    echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"Submit\">";     
    echo "</form>"; 
    echo "</div>"; 
  }
}

function MakeFormG($pos, $type)
{
  $response = IsRespondedG($pos);
  if(!$response)
  {
    echo "<div class=\"responseDiv\" id=\"res" . $pos . "\" style=\"display: none\">";
    
    echo "<div class=\"response\">";
    echo "<p>This must be hidden</p>";          
    echo "</div>";  
    echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>"; 
    echo "</div>";       

    echo "<div id=\"form" . $pos . "\">";
    
    echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponse" . $type . ".php\" method=\"post\">";
    echo "<div>";
    echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
    echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" ></textarea>";    
    echo "</div>";    
    echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"Submit\">";     
    echo "</form>";  
    echo "</div>";    
  }
  else
  {
    echo "<div class=\"responseDiv\" id=\"res" . $pos . "\">";
    
    echo "<div class=\"response\">";
    echo "<p>" . FillItG($pos) . "</p>";
    echo "</div>";
    echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>"; 
    echo "</div>"; 


    echo "<div id=\"form" . $pos . "\" style=\"display: none\">";
    
    echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponse" . $type . ".php\" method=\"post\">";
    echo "<div>";
    echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
    echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" >" . $response[0][0] . "</textarea>";    
    echo "</div>";    
    echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"Submit\">";     
    echo "</form>"; 
    echo "</div>"; 
  }
}

function MakeFormGAJAX($pos, $type)
{
  $response = IsRespondedG($pos);
  if(!$response)
  {
    echo "<div class=\"responseDiv\" id=\"res" . $pos . "\" style=\"display: none\">";
    
    echo "<div class=\"response\">";
    echo "<p id=\"p" .$pos . "\">This must be hidden</p>";          
    echo "</div>";  
    echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>"; 
    echo "</div>";       

    echo "<div id=\"form" . $pos . "\">";
    
    echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"\" method=\"post\">";
    echo "<div>";
    echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
    echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" ></textarea>";    
    echo "</div>";    
    echo "<input id=\"task_pos\" type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";
    echo "<input type=\"button\" value=\"Submit\" onclick=\"loadDoc('" . $pos . "','" . $type . "')\">"; 
    echo "</form>";  
    echo "</div>";    
  }
  else
  {
    echo "<div class=\"responseDiv\" id=\"res" . $pos . "\">";
    
    echo "<div class=\"response\">";
    echo "<p id=\"p" .$pos . "\"></p>";
    echo "</div>";
    echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>"; 
    echo "</div>"; 


    echo "<div id=\"form" . $pos . "\" style=\"display: none\">";
    
    echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"\" method=\"post\">";
    echo "<div>";
    echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
    echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" >" . $response[0][0] . "</textarea>";    
    echo "</div>";
    echo "<input id=\"task_pos\" type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
    echo "<input type=\"button\" value=\"Submit\" onclick=\"loadDoc('" . $pos . "','" . $type . "')\">";     
    echo "</form>"; 
    echo "</div>"; 
  }
}


function IsResponded($position)
{
  $response = null;
  $handle = CreateHandle();
  
  $userID = $_SESSION['user_id'];
  $sql_response = "SELECT res_text, res_submit_date FROM responses WHERE res_position='$position' AND fk_user_id='$userID'";

  $response = MyQueryDB($handle, $sql_response);

  return $response;
}

function IsRespondedG($position)
{
  $response = null;
  $handle = CreateHandle();
  $group = $_SESSION['user_group'];
  
  $userID = $_SESSION['user_id'];
  $sql_response = "SELECT g_res_text, g_res_date FROM group_response WHERE g_res_position='$position' AND g_id='$group' ORDER BY g_res_position";

  $response = MyQueryDB($handle, $sql_response);

  return $response;
}


function FillIt($position)
{
  $response = IsResponded($position);
            
  if((count($response)>0) && $response)
  {
    echo $response[0][0];
  }
}

function FillItG($position)
{
  $response = IsRespondedG($position);
            
  if((count($response)>0) && $response)
  {
    echo $response[0][0];
  }
}

function IsChecked($pos)
{
  $handle = CreateHandle();
  $isSubmitted = null;
  $userID = $_SESSION['user_id'];

  $sql_isSubmitted = "SELECT res_text FROM responses WHERE fk_user_id='$userID' AND res_position='$pos'";
            


  $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
  if(count($isSubmitted)>0)
  {
    return "checked";
  }
  else
  {
    return "unchecked";
  }
}

function PresentLearned()
{
  $learned = FindResponseByKind("W");
  $lst = $learned[count($learned) - 1][2];
  $i = substr($lst, -2);
  $i++;
  $done = false;
  while(!$done)
  {
    
    if($i < 10){
      $pos = "firstL0" . $i;
    }
    else
    {
      $pos = "firstL" . $i;
    }
    
    $n = $i - 3;

    $handle = CreateHandle();

    $isSubmitted = null;
    $group = $_SESSION['user_group'];

    $sql_isSubmitted = "SELECT g_res_text FROM group_response WHERE g_id='$group' AND g_res_position='$pos'";
            


    $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
    if(count($isSubmitted)<1)
    {
      $done = true;
    }
    else
    {
      echo "<div class=\"responseDiv\" id=\"res" . $pos . "\">";
      echo "<h4>Fact " . $i . "</h4>";
      echo "<div class=\"response\">";
      echo "<p>" . FillItG($pos) . "</p>";     
      echo "</div>";
      echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>";
      echo "</div>";  

      echo "<div id=\"form" . $pos . "\" style=\"display: none\">";
      echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponseL.php\" method=\"post\">";
      echo "<div>";
      echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
      echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" >" . $isSubmitted[0][0] . "</textarea>";    
      echo "</div>";    
      echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"Submit\">";     
      echo "</form>";
      echo "</div>";  
      echo "<br/>";  
    }
    $i++;  

  }
  $i--;
  if($i < 10){
      $pos = "firstL0" . $i;
    }
    else
    {
      $pos = "firstL" . $i;
    }
  echo "<h4>Add your question here:</h4>";
  echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponseL.php\" method=\"post\">";
  echo "<div>";
  echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
  echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" ></textarea>";    
  echo "</div>";    
  echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"Submit\">";     
  echo "</form>";    
}

function PresentQuestions()
{
  $i = 4;
  $done = false;
  while(!$done)
  {
    if($i < 10)
    {
      $pos = "firstW0" . $i;
    }
    else
    {
      $pos = "firstW" . $i;
    }

    //$pos = "firstW" . $i;
    $n = $i - 3;

    $handle = CreateHandle();

    $isSubmitted = null;
    $userID = $_SESSION['user_id'];

    $sql_isSubmitted = "SELECT res_text FROM responses WHERE fk_user_id='$userID' AND res_position='$pos'";
            


    $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
    if(count($isSubmitted)<1)
    {
      $done = true;
    }
    else
    {
      echo "<div class=\"responseDiv\" id=\"res" . $pos . "\">";
      echo "<h4>Question " . $n . "</h4>";
      echo "<div class=\"response\">";
      echo "<p>" . FillIt($pos) . "</p>";     
      echo "</div>";
      echo "<input type=\"button\" value=\"Edit\" onclick=\"edit('" . $pos . "')\"/>";
      echo "</div>";  

      echo "<div id=\"form" . $pos . "\" style=\"display: none\">";
      echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponseW.php\" method=\"post\">";
      echo "<div>";
      echo "<input type=\"hidden\" name=\"position\" value=\"" . $pos . "\">";    
      echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" >" . $isSubmitted[0][0] . "</textarea>";    
      echo "</div>";    
      echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"submit your question\">";     
      echo "</form>";
      echo "</div>";  
      echo "<br/>";  
    }
    $i++;  

  }
  $i--;
  if($i < 10)
    {
      $pos = "firstW0" . $i;
    }
    else
    {
      $pos = "firstW" . $i;
    }
  echo "<h4>Add your question here:</h4>";
  echo "<form id=\"" . $pos . "\" name=\"" . $pos . "\" action=\"processResponseW.php\" method=\"post\">";
  echo "<div>";
  echo "<input type=\"hidden\" name=\"position\" value=\"firstW" . $i . "\">";    
  echo "<textarea id=\"" . $pos . "Txt\" name=\"" . $pos . "Txt\" cols=\"40\" rows=\"5\" ></textarea>";    
  echo "</div>";    
  echo "<input type=\"submit\" id=\"" . $pos . "Submit\" name=\"" . $pos . "Submit\" value=\"submit your question\">";     
  echo "</form>";    
}

function GetQuestions()
{
  $counter =0;
 $questions = FindResponseByKind("W");
 for($i=1; $i<count($questions) + 1; $i++)
 {
  $posW = $questions[$i - 1][2];
  $pos = "firstL" . substr($posW, -2);

  echo "<h3>" . $questions[$i - 1][0] . "</h3>";

  MakeFormGAJAX($pos, "L");
  echo "<br/>";
  echo "<input id=\"task_no" . $i . "\" type=\"hidden\" name=\"position\" value=\"" . $pos . "\">"; 
  $counter = substr($posW, -2);
 }
 echo "<input id=\"task_counter\" type=\"hidden\" name=\"position\" value=\"" . $counter . "\">"; 
}

function AddForm($type, $juncType)
{
  $i = CountResponseByKind($juncType) + 1;
  echo "<form id=\"firstL" . $i . "\" name=\"firstL" . $i . "\" action=\"processResponseL.php\" method=\"post\">";
  echo "<div>";
  echo "<input type=\"hidden\" name=\"position\" value=\"firstL" . $i . "\">";    
  echo "<textarea id=\"firstL" . $i . "Txt\" name=\"firstL" . $i . "Txt\" cols=\"40\" rows=\"5\" ></textarea>";    
  echo "</div>";    
  echo "<input type=\"submit\" id=\"firstL" . $i . "Submit\" name=\"firstL" . $i . "Submit\" value=\"submit your question\">";     
  echo "</form>"; 
}

function CountResponseByKind($type)
{
  $responses = FindResponseByKind($type);
  if(count($responses > 0))
  {
    return count($responses);
  }
  else
  {
    return 0;
  }
}

function FindResponseByKind($type)
{
  $response = null;
  $counter = 0;
  $result = null;
  $handle = CreateHandle();
  
  $group = $_SESSION['user_group'];
  $sql_response = "SELECT g_res_text, g_res_date, g_res_position FROM group_response WHERE g_id='$group' AND g_res_type='$type' ORDER BY g_res_position";

  $response = MyQueryDB($handle, $sql_response);

  return $response;
  
}

function MakeItInvisible($task, $thisTask)
{
  
  
  if($task == null)
  {
    if($thisTask == "0")
    {
      return "block";
    }
    else
    {
      return "none";
    }
  }
  else
  {
    if($task == $thisTask)
    {
      return "block";
    }
    else
    {
      if($thisTask == "0")
      {
        switch ($task) 
        {
          case "1":
            return "none";        
          case "2":
            return "none"; 
          case "3":
            return "none"; 
          case "4":
            return "none"; 
          case "5":
            return "none"; 
          case "":
            return "none"; 
          default:
              return "block";
        }
      }
      else
      {
        return "none";
      }
    }
  }
}

function PresentKTask($pos)
{
  if($pos=="firstK1")
  {
    echo "<h3>The Ball</h3>";            
    echo"<div class=\"pQuestion\">";
    echo "<p>Look at this ball, it stays at its position. Do you think is it possible that the ball start to move by itself? What should be happened to move the ball?</p>";
    echo "</div>";
    echo "<div class=\"imgQ\">";
    echo "<img src=\"Files/ball.jpg\" class=\"imgGrid\">";
    echo "</div>";
  }
  elseif($pos=="firstK2")
  {
    echo "<h3>Football Player</h3>";
    echo "<div class=\"pQuestion\">";
    echo "<p>When a football player kick a ball, what causes the ball to move?</p>";
    echo "</div>";
    echo "<div class=\"imgQ\">";
    echo "<img src=\"Files/kicking.jpg\" class=\"imgGrid\">";
    echo "</div>";
  }
  elseif($pos=="firstK3")
  {
    echo "<h3>The Ball and astronaut</h3>";
    echo "<div class=\"pQuestion\">";
    echo "<p>Watch this video, then explain what would happens if an astronaut throws a ball into the space?</p>";
    echo "</div>";
    echo "<div class=\"videoGridCorner\">";
    echo "<video id=\"video1\" style=\"width:300px;max-width:100%;\" controls=\"\">";
    echo "<source src=\"https://upload.wikimedia.org/wikipedia/commons/transcoded/1/15/Station_Astronauts_Do_Experiment_for_%27Cosmos%27.webm/Station_Astronauts_Do_Experiment_for_%27Cosmos%27.webm.360p.webm\" type=\"video/webm\">";
    echo "Your browser does not support HTML5 video.";
    echo "</video>";
    echo "</div>";
  }
  elseif($pos=="firstK4")
  {
    echo "<h3>What do you know about inertia?</h3>";
    echo "<p>Do you know what inertia is? Write anything which it bears in your mind.</p>";
  }
  elseif($pos=="firstK5")
  {
    echo "<h3>The Definition</h3>";
    echo "<p>This is the definition of <b>First Law of Motion</b>, read it and response to these questions:</p>";
    echo "<p class=\"definition\"><b>An object at rest stays at rest and an object in motion stays in motion with the same speed and in the same direction unless acted upon by an unbalanced force</b></p>";
    echo "<p>What this definition means to you? Do you know the terms which are used in it?</p>";
    echo "<p>Write anything you know and keep in mind your questions for the next session.</p>";
  }
}




