<?php

session_start();
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';


if (!isset($_SESSION['userName']))
{
    /*
     * this mimics a login check- if the session variable isn't set, then we send to the start page (in a live
     * system this would be a login page
     */
    header('Location: login.php');
    die();

}

UpdateSession();
 //set the variable $errMsg to null- this is what we will use to check if errors have been detected later in the page.
$errMsg = null;
// check that data has been posted
if (isset($_POST['newMsg']))
{
    //validate the message
    if (MsgIsValid($_POST['newMsg'], 1, 1000))
    {
        //insert the new post to the database
        $handle = CreateHandle();
        //escape potentially dangerous characters
        $msg = $handle->real_escape_string($_POST['newMsg']);
        $nm = $handle -> real_escape_string($_SESSION['userName']);
        $pos = $handle -> real_escape_string($_POST['task']);
        $group = $handle -> real_escape_string($_SESSION['user_group']);
        $user_id = $handle -> real_escape_string($_SESSION['user_id']);
        //create the sql statement (note this is particularly insecure)
        $sql = "INSERT INTO tasks_chat_msgs (postedBy, chat_msg_text, chat_msg_pos, chat_msg_group, chat_msg_user_id) VALUES ('" . $nm . "', '" .$msg . "', '" . $pos . "', '" . $group . "', '" . $user_id . "')";
        //we can store the result of the query in a variable and then write code to respond depending on the result- this is omitted here for clarity
        $inserted =  $handle -> query($sql) or die(mysqli_error($handle));
          //we will need to refresh the chat page and not re-post data, so we use this page to handle the posted message
        // and then return to the chat page
        header('Location: chat_k.php?task=' . $_POST['task']);
    }
    else
    {
        //there is a problem with the characters in the message or the message is blank, so create the error message to display to users
        $errMsg =  "<p>Please check what you have typed- it may contain invalid characters</p>";
    }
}


//we only render the html page if the chat message doesn't validate.

?>
<!doctype html>
    <html>
<head>
    <title>Chat</title>

    <style type="text/css">


    </style>
</head>
    <body>
    <h1>Chat example</h1>



    <?php
    echo $errMsg;
//this form is identical to the one in chat.php, but here we are only handling messages that have errors
     //we need this  separate page to handle the messages being posted (or re-posted if there are errors) so that the
    // chat.php page can safely be refreshed.

    ?>
    <form id="chatMessage" name="chatMessage" action="processMsg.php" method="post">

        <div>
            <input type="hidden" name="task" value=<?php echo "\"" . $_POST['task'] . "\""; ?>>
        <label for="newMsg">Your message</label>
        <textarea id="newMsg" name="newMsg" cols="20" rows="5" ><?php echo isset($errMsg) ? htmlspecialchars($_POST['newMsg']) : "" ?></textarea>
            </div>
        <input type="submit" id="cmdSubmit" name="cmdSubmit" value="Add chatMessage">
    </form>
    </body>
    </html>