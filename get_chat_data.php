<?php

session_start();

if (!isset($_SESSION['userName'])) {
  //if the user has already provided a name, then redirect them to the chat page
    header('Location: login.php');
    die();

}
require_once 'app_lib.php';

$group = $_SESSION['user_group'];
$task_pos = $_POST['task'];

  $handle = CreateHandle();


    $sql = "SELECT chat_msg_text, postedBy, timedAt FROM tasks_chat_msgs WHERE chat_msg_pos='$task_pos' AND chat_msg_group='$group' ORDER BY timedAt";
       //pass the queryDB() function the handle and sql statement we need to access the database and retrieve existing messages
    $chatMsgs = queryDB($handle, $sql);

    if (count($chatMsgs)<1)
    {
       echo "No messages have been posted, be the first to post one!";
    }
    else
    {
        
          //now output all the messages to the page
        for ($i=0; $i<count($chatMsgs); $i++)
        {
              //format the date so we are only showing the time the chat message was posted.
            $dateTimePosted = strtotime( $chatMsgs[$i][2]);
            $showDate = date('H:i', $dateTimePosted);
              //write the results (unstyled) to the page
            echo "<div class=\"comment\"><p>" . $chatMsgs[$i][1] . " said: <br><b>" . $chatMsgs[$i][0] . "</b><br>" . $showDate . "</p></div>";
        }
    }
?>