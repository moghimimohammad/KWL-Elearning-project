<?php

session_start();
//notice that we need to call the session_start() function in all pages to use session vars

// include common functions that other pages will need.
require_once 'app_lib.php';

UpdateSession();


if (!isset($_SESSION['userName']))
{
    /*
     * this mimics a login check- if the session variable isn't set, then we send to the start page (in a live
     * system this would be a login page
     */
    header('Location: login.php');
    die();

}


 //set the variable $errMsg to null- this is what we will use to check if errors have been detected later in the page.
$position = null;
$errMsg = null;
$text = null;
$group = null;

$checked = array();
if(isset($_SESSION['user_group']))
{
    $group = $_SESSION['user_group'];
   
}

$counter = (isset($_POST['counter'])) ? $_POST['counter'] : 0;

for($i = 1; $i < $counter + 1; $i++)
{
    if(i<10)
    {
        $pos = "firstW0" . $i;
        $pos_l = "firstL0" . $i;
    }
    else
    {
        $pos = "firstW" . $i;
        $pos_l = "firstL" . $i;
    }
    
   
    if (isset($_POST[$pos]))
    {

    


        

        if (MsgIsValid($_POST[$pos], 1, 1000))
        {
            //insert the new post to the database
            $handle = CreateHandle();

            $isSubmitted = null;
            $userID = $_SESSION['user_id'];

            $sql_isSubmitted = "SELECT g_res_text FROM group_response WHERE g_id='$group' AND g_res_position='$pos'";
            


            $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
            if(count($isSubmitted)<1)
            {
                //escape potentially dangerous characters
            $msg = $handle->real_escape_string($_POST[$pos]);
            $user_id = $handle -> real_escape_string($_SESSION['user_id']);
            //create the sql statement (note this is particularly insecure)
            $sql = "INSERT INTO group_response (g_id, g_res_position, g_res_text, g_res_type) VALUES ('" . $group . "', '" . $pos . "', '" .$msg . "', 'W')";
            //we can store the result of the query in a variable and then write code to respond depending on the result- this is omitted here for clarity
            $inserted =  $handle -> query($sql) or die(mysqli_error($handle));
              //we will need to refresh the chat page and not re-post data, so we use this page to handle the posted message
            // and then return to the chat page
            }
            else
            {
                $msg = $handle->real_escape_string($_POST[$pos]);
                $user_id = $handle -> real_escape_string($_SESSION['user_id']);
                
                $sql_update = "UPDATE group_response SET g_res_text='$msg' WHERE g_id='$group' AND g_res_position='$pos'";
                
                $checked[] = $pos; 
                if (mysqli_query($handle, $sql_update)) {
                    //echo "Record updated successfully";
                } else
                 {
                    //echo "Error updating record: " . mysqli_error($conn);
                }
            }
            
            //header('Location: first_law_w.php');
        }
        else
        {
            //there is a problem with the characters in the message or the message is blank, so create the error message to display to users
            $errMsg =  "<p>Please check what you have typed- it may contain invalid characters</p>";
            
        }
    } 
    else
    {
        $handle = CreateHandle();
        $isSubmitted = null;
        $userID = $_SESSION['user_id'];

        $sql_isSubmitted = "SELECT g_res_text FROM group_response WHERE g_id='$group' AND g_res_position='$pos'";
            


        $isSubmitted = MyQueryDB($handle , $sql_isSubmitted);
            
        if(count($isSubmitted)>0)
        {

            $userID = $_SESSION['user_id'];
            $sql_update = "DELETE FROM group_response WHERE g_id='$group' AND g_res_position='$pos'";
                
            if (mysqli_query($handle, $sql_update)) {
                    //echo "Record updated successfully";
            } else
            {
                    //echo "Error updating record: " . mysqli_error($conn);
            }
            
            $sql_update_l = "DELETE FROM group_response WHERE g_id='$group' AND g_res_position='$pos_l'";
            if (mysqli_query($handle, $sql_update_l)) {
                    //echo "Record updated successfully";
            } else
            {
                    //echo "Error updating record: " . mysqli_error($conn);
            }
        }

    }
}
//header('Location: first_law_w.php');
echo json_encode($checked);

//we only render the html page if the chat message doesn't validate.

?>
